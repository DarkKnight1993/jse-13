package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.api.service.ITaskService;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String name) {
        if (name == null || name.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public boolean existsById(String id) {
        if (id == null || id.isEmpty()) return false;
        return taskRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(Integer index) {
        if (index == null || index < 0) return false;
        return taskRepository.existsByIndex(index);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public Task findByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.startById(id);
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.startByIndex(index);
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.startByName(name);
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.finishById(id);
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.finishByIndex(index);
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.finishByName(name);
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeStatusById(id, status);
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        if (status == null) return null;
        return taskRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Task changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        if (status == null) return null;
        return taskRepository.changeStatusByName(name, status);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
