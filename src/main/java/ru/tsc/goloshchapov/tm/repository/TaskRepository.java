package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.ITaskRepository;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public boolean existsById(String id) {
        final Task task = findById(id);
        return task != null;
    }

    @Override
    public boolean existsByIndex(Integer index) {
        if (index < 0) return false;
        return index < tasks.size();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findById(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task startById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String name, final Status status) {
        final Task task = findByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        List<Task> listByProject = new ArrayList<>();
        for (Task task : tasks) {
            if (projectId.equals(task.getProjectId())) listByProject.add(task);
        }
        return listByProject;
    }

    @Override
    public void removeAllTaskByProjectId(final String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(projectId);
        for (Task task : listByProject) {
            tasks.remove(task);
        }
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String taskId) {
        final Task task = findById(taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
