package ru.tsc.goloshchapov.tm.controller;

import ru.tsc.goloshchapov.tm.api.controller.IProjectController;
import ru.tsc.goloshchapov.tm.api.service.IProjectService;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            System.out.println(index + ") " + project.toString());
            index++;
        }
        System.out.println("[END LIST]");
    }

    public void showProject(Project project) {
        System.out.println("[SELECTED PROJECT]");
        System.out.println(
                "Id: " + project.getId() +
                        "\nName: " + project.getName() +
                        "\nDescription: " + project.getDescription() +
                        "\nStatus: " + project.getStatus()
        );
        System.out.println("[END PROJECT]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showProject(project);
    }

    @Override
    public void showByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        showProject(project);
    }

    @Override
    public void updateById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateById(id, name, description);
        if (projectUpdated == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void updateByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = projectService.updateByIndex(index, name, description);
        if (projectUpdated == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void removeById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!projectService.existsById(id)) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        final Project projectRemoved = projectService.removeById(id);
        if (projectRemoved == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void removeByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!projectService.existsByIndex(index)) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        final Project projectRemoved = projectService.removeByIndex(index);
        if (projectRemoved == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void removeByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        if (project == null) {
            System.out.println("INCORRECT VALUES!");
            return;
        }
        final Project projectRemoved = projectService.removeByName(name);
        if (projectRemoved == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void startById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void startByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startByIndex(index);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void startByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void finishById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void finishByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.finishByIndex(index);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void finishByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void changeStatusById() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusById(id, status);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByIndex(index, status);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

    @Override
    public void changeStatusByName() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Project project = projectService.changeStatusByName(name, status);
        if (project == null) System.out.println("INCORRECT VALUES!");
    }

}
