package ru.tsc.goloshchapov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

    void showById();

    void showByIndex();

    void showByName();

    void updateById();

    void updateByIndex();

    void removeById();

    void removeByIndex();

    void removeByName();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
